// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

unit utils;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Process, LazFileUtils, Forms,

  // FPC trunk fileinfo reads exe resources as long as you register the appropriate units
  winpeimagereader {needed for reading exe info}, elfreader {for reading ELF executables}, machoreader {reading MACH-O executables};

const
  COL_REPLACE     = 0;  // columns in CSV database file
  COL_HANDLER     = 1;
  COL_LAZARUS     = 2;
  COL_TYPHON      = 3;
  COL_DESCRIPTION = 4;

  HANDLER_NONE                         = 0;
  HANDLER_FORCE_REPLACE_WITHOUT_QUOTES = 1;
  HANDLER_FORCE_PARTIAL_STRING_REPLACE = 2;

  TABS = '   ';

procedure Log(const LogMsg: UTF8String);
procedure LogTab(const LogMsg: UTF8String);
procedure LogTabTab(const LogMsg: UTF8String);
procedure LogDateTime(const LogMsg: UTF8String);
procedure LogStep(const StepNo: byte; const LogMsg: UTF8String);
procedure LogOverwrite(const LogMsg: UTF8String);
procedure LogTabOverwrite(const LogMsg: UTF8String);

function  IsWholeWord(var SourceStr, FoundStr: string; const FoundStrPos: integer): boolean;
function  MyStringReplace(const Source, OldPattern, NewPattern: string; Flags: TReplaceFlags; const WholeWordsOnly: boolean; out FlagReplaced: boolean): string;
function  ReplaceStr(const SourceString: string; SearchString, ReplaceString: string; const UseQuotes, UseWholeWordReplacementsOnly: boolean; var Replaced: boolean): string;
function  StringWithoutFirstChar(const SourceString: string): string;
function  ReplaceMacros(const SourceString: UTF8String): UTF8String;
function  IsBlankInPath(const PathStr: string): boolean;

function  GetExtractDir: UTF8String;
function  GetComponentsFullFileName(ANumericExtension: UTF8String): UTF8String;
function  GetExamplesFullFileName(ANumericExtension: UTF8String): UTF8String;

function  DelBSpace(const s: string): string;
function  Copy2Symb(const s: string; Symb: char): string;
function  Copy2SymbDel(var s: string; Symb: char): string;

procedure RunProcess(ExecCmd: string);

implementation

uses
  mainform;


procedure Log(const LogMsg: UTF8String);
begin
  FormConverter.LogMemo.Append(LogMsg);
end;

procedure LogTab(const LogMsg: UTF8String);
begin // indent small
  Log(TABS + LogMsg);
end;

procedure LogTabTab(const LogMsg: UTF8String);
begin // indent big
  Log(TABS + TABS + LogMsg);
end;

procedure LogDateTime(const LogMsg: UTF8String);
begin
  Log(DateTimeToStr(Now) + '   ' + LogMsg);
end;

procedure LogStep(const StepNo: byte; const LogMsg: UTF8String);
begin
  LogDateTime('Step ' + IntToStr(StepNo) + ': ' + ' ' + LogMsg);
end;

procedure LogOverwrite(const LogMsg: UTF8String);
begin
  FormConverter.LogMemo.Lines.BeginUpdate;
  try
    FormConverter.LogMemo.Lines[FormConverter.LogMemo.Lines.Count - 1] := LogMsg;
  finally
    FormConverter.LogMemo.Lines.EndUpdate;
  end;
end;

procedure LogTabOverwrite(const LogMsg: UTF8String);
begin
  LogOverwrite(TABS + LogMsg);
end;


{ String handling }

function IsWholeWord(var SourceStr, FoundStr: string; const FoundStrPos: integer): boolean;
const // when FoundStr was found in SourceStr, call this function to check if FoundStr was whole string or just part of a bigger string
  WordChars: set of char = ['a'..'z','A'..'Z','0'..'9','_'];
var
  IsPrefixPartOfTheWord, IsSufixPartOfTheWord: boolean;
  Len: integer;
begin
  Len := Length(FoundStr);
  IsPrefixPartOfTheWord := False;
  IsSufixPartOfTheWord  := False;

  if FoundStrPos > 1 then
    if (SourceStr[FoundStrPos - 1] in WordChars) then
      IsPrefixPartOfTheWord := True;

  if (FoundStrPos + Len) <= Length(SourceStr) then
    if (SourceStr[FoundStrPos + Len] in WordChars) then
      IsSufixPartOfTheWord := True;

  Result := not (IsPrefixPartOfTheWord or IsSufixPartOfTheWord);
end;

// todo: Investigate potential speed improvement by searching all patterns at once, by using StrUtils.StringsReplace
function MyStringReplace(const Source, OldPattern, NewPattern: string; Flags: TReplaceFlags; const WholeWordsOnly: boolean; out FlagReplaced: boolean): string;
var // modified SysUtils.StringReplace from file sysstr.inc
  RemainingSrcStr, PartialSrcStr, OldPatternStr: string; // SourceStr can contain uppercase versions of Source
  PatternFoundPosition: integer;
  WholeWordFound: boolean;
begin
  Result := '';
  PartialSrcStr := Source;
  FlagReplaced := False; // to indicate if any replacement was done in this function call

  if rfIgnoreCase in Flags then
  begin
    RemainingSrcStr := AnsiUpperCase(Source);
    OldPatternStr   := AnsiUpperCase(OldPattern);
  end
  else
  begin
    RemainingSrcStr := Source;
    OldPatternStr   := OldPattern;
  end;

  while (Length(RemainingSrcStr) <> 0) do
  begin
    PatternFoundPosition := AnsiPos(OldPatternStr, RemainingSrcStr);
    if PatternFoundPosition = 0 then
    begin
      Result := Result + PartialSrcStr;
      RemainingSrcStr := '';
    end
    else
    begin
      WholeWordFound := True;
      if WholeWordsOnly then
        if not IsWholeWord(RemainingSrcStr, OldPatternStr, PatternFoundPosition) then // check if we have whole word or just part of a bigger word
          WholeWordFound := False;

      if WholeWordsOnly and (not WholeWordFound) then
        Result := Result + Copy(PartialSrcStr, 1, PatternFoundPosition - 1) + OldPattern  // skip this new match since only whole words are matched
      else
      begin
        Result := Result + Copy(PartialSrcStr, 1, PatternFoundPosition - 1) + NewPattern; // we have a match so replace old pattern with new one
        FlagReplaced := True; // indicate that there was at least one replacement in this function call
      end;

      PatternFoundPosition := PatternFoundPosition + Length(OldPatternStr);
      PartialSrcStr        := Copy(PartialSrcStr, PatternFoundPosition, Length(PartialSrcStr) - PatternFoundPosition + 1);
      if not (rfReplaceAll in Flags) then
      begin
        Result := Result + PartialSrcStr;
        RemainingSrcStr := '';
      end
      else
        RemainingSrcStr := Copy(RemainingSrcStr, PatternFoundPosition, Length(RemainingSrcStr) - PatternFoundPosition + 1);
    end;
  end;
end;

function ReplaceStr(const SourceString: string; SearchString, ReplaceString: string; const UseQuotes, UseWholeWordReplacementsOnly: boolean; var Replaced: boolean): string;
var
  i: integer;
  FlagReplaced: boolean;
begin
  SearchString  := Trim(SearchString);
  ReplaceString := Trim(ReplaceString);

  if UseQuotes then
  begin
    SearchString  := AnsiQuotedStr(SearchString, '"');
    ReplaceString := AnsiQuotedStr(ReplaceString, '"');
  end;

  i := Pos(AnsiUpperCase(SearchString), AnsiUpperCase(SourceString));
  if i > 0 then
  begin
    Result := MyStringReplace(SourceString, SearchString, ReplaceString, [rfReplaceAll, rfIgnoreCase], UseWholeWordReplacementsOnly, FlagReplaced);
    if FlagReplaced then
    begin
      Replaced := True; // to decide later if file saving is needed
      LogTabTab(SearchString + ' >>> ' + ReplaceString);
    end;
  end
  else
    Result := SourceString;
end;

function StringWithoutFirstChar(const SourceString: string): string;
begin
  if Length(SourceString) <= 1 then
    Result := ''
  else
    Result := RightStr(SourceString, Length(SourceString) - 1);
end;

function ReplaceMacros(const SourceString: UTF8String): UTF8String;
const
  MACRO_COMPONENTS_FULL_FILE_NAME = '%COMPONENTS';
  MACRO_EXAMPLES_FULL_FILE_NAME   = '%EXAMPLES';
  MACRO_EXTRACT_DIR_PATH          = '%EXTRACTDIR';
var
  ReplaceString: UTF8String;
begin
  with FormConverter do
  begin
    Result := SourceString;

    ReplaceString := GetComponentsFullFileName('.001'); // Since CT 7.20 7zip files have numeric extensions 001, 002, 003..., but 7zip extraction needs just first file
    Result := StringReplace(Result, MACRO_COMPONENTS_FULL_FILE_NAME, ReplaceString, [rfReplaceAll, rfIgnoreCase]);

    ReplaceString := GetExamplesFullFileName('.001'); // Since CT 7.20 7zip files have numeric extensions 001, 002, 003..., but 7zip extraction needs just first file
    Result := StringReplace(Result, MACRO_EXAMPLES_FULL_FILE_NAME,   ReplaceString, [rfReplaceAll, rfIgnoreCase]);

    ReplaceString := GetExtractDir;
    Result := StringReplace(Result, MACRO_EXTRACT_DIR_PATH,          ReplaceString, [rfReplaceAll, rfIgnoreCase]);
  end;
end;

function IsBlankInPath(const PathStr: string): boolean;
begin
  if Pos(' ', PathStr) > 0 then
    Result := True
  else
    Result := False;
end;

function GetExtractDir: UTF8String;
begin
  Result := TrimFileName(FormConverter.ExtractDir.Text);
end;

function GetComponentsFullFileName(ANumericExtension: UTF8String): UTF8String;
begin
  Result := TrimFileName(GetExtractDir + PathDelim + ExtractFileName(FormConverter.ComponentsURL.Text + ANumericExtension));
end;

function GetExamplesFullFileName(ANumericExtension: UTF8String): UTF8String;
begin
  Result := TrimFileName(GetExtractDir + PathDelim + ExtractFileName(FormConverter.ExamplesURL.Text + ANumericExtension));
end;


{ Copied deprecated functions from rxstrutils }

function DelBSpace(const s: string): string;
var
  i, l: integer;
begin
  l := Length(s);
  i := 1;
  while (i <= l) and (s[i] = ' ') do
    Inc(i);
  Result := Copy(s, i, MaxInt);
end;

function Copy2Symb(const s: string; Symb: char): string;
var
  p: integer;
begin
  p := Pos(Symb, s);
  if p = 0 then
    p := Length(s) + 1;
  Result := Copy(s, 1, p - 1);
end;

function Copy2SymbDel(var s: string; Symb: char): string;
begin
  Result := Copy2Symb(s, Symb);
  s := DelBSpace(Copy(s, Length(Result) + 2, Length(s))); // fixed symbol char not deleted (+2)
end;


{ Run external process and capture it's output }

procedure RunProcess(ExecCmd: string);
const
  READ_BYTES = 2048;
var
  OutputLines: TStringList;
  MemStream:   TMemoryStream;
  OurProcess:  TProcess;
  NumBytes, NumLines, BytesRead: LongInt;
begin
  MemStream  := TMemoryStream.Create; // A temp Memorystream is used to buffer the output
  OurProcess := TProcess.Create(nil);
  BytesRead  := 0;

  OurProcess.Parameters.Clear;
  {$IFDEF WINDOWS}
  // cmd.exe /c "c:\Program Files\7-Zip\7z.exe" -y x %COMPONENTS typhon/components/* -r -o%EXTRACTDIR
  OurProcess.Executable := 'cmd.exe';
  OurProcess.Parameters.Add('/c');
  {$ELSE} // UNIX
  // sh -c 7z -y x %COMPONENTS typhon/components/* -r -o%EXTRACTDIR
  OurProcess.Executable := 'sh';
  OurProcess.Parameters.Add('-c');
  {$ENDIF}
  OurProcess.Parameters.Add(ExecCmd);
  // LogTab('Executable: ' + OurProcess.Executable);
  // LogTab('Parameters: ' + OurProcess.Parameters.Text);
  OurProcess.Options := [poUsePipes];
  OurProcess.ShowWindow := swoMinimize;
  LogTab('Extraction command: ' + OurProcess.Executable + ' ' + OurProcess.Parameters[0] + ' ' + OurProcess.Parameters[1]);
  LogTab('External program run started. Collected 0 bytes from it''s output...');
  OurProcess.Execute;
  while True do
  begin
    Application.ProcessMessages;
    MemStream.SetSize(BytesRead + READ_BYTES); // make sure we have room
    NumBytes := OurProcess.Output.Read((MemStream.Memory + BytesRead)^, READ_BYTES); // try reading it
    if NumBytes > 0 then // All read() calls will block, except the final one.
    begin
      Inc(BytesRead, NumBytes);
      FormConverter.LogMemo.Lines.BeginUpdate;
      FormConverter.LogMemo.Lines[FormConverter.LogMemo.Lines.Count-1] := '   External program run started. Collected ' + IntToStr(BytesRead) + ' bytes from it''s output...';
      FormConverter.LogMemo.Lines.EndUpdate;
    end
    else
      break; // external program has finished execution
  end;

  If BytesRead < 1000 then
    LogTab('Unusually small number of bytes processed. Check if you have latest 7zip version installed' {$IFDEF WINDOWS} + ', and in default location!' {$ELSE} + '!' {$ENDIF});

  MemStream.SetSize(BytesRead);
  LogTab('External program run complete');

  if BytesRead < 1000 then // if there are not megabytes of data then there was probably some error, so display all output
  begin
    OutputLines := TStringList.Create;
    OutputLines.LoadFromStream(MemStream);
    LogTab('External program output start (' + IntToStr(OutputLines.Count) + ' lines):');
    for NumLines := 0 to OutputLines.Count - 1 do
      LogTabTab(OutputLines[NumLines]);
    if OutputLines.Count = 0 then
      LogTab('ERROR: Check if 7zip is installed and that path to executable is correct.');
    LogTab('External program output end'); // this line is too slow when done zillion times, so we only do this if we think there might be an error (BytesRead < 1000)
    OutputLines.Free;
  end;
  OurProcess.Free;
  MemStream.Free;
end;

end.

