// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

unit downloader;

// download http file with progres indicator
// http://forum.lazarus.freepascal.org/index.php/topic,21824.msg218346.html

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type
  TOnProgress = procedure(Sender: TObject; Percent: integer) of object;

  { TStreamAdapter }

  TStreamAdapter = class(TStream)
  strict private
    fOnProgress: TOnProgress;
    fPercent: integer;
    fStream: TStream;
  public
    constructor Create(AStream: TStream; ASize: int64);
    destructor  Destroy; override;
    function    Read(var Buffer; Count: longint): longint; override;
    function    Write(const Buffer; Count: longint): longint; override;
    function    Seek(Offset: longint; Origin: word): longint; override;
    procedure   DoProgress; virtual;
  published
    property    OnProgress: TOnProgress read FOnProgress write FOnProgress;
  end;


procedure DonwloadFile(OnProgress: TOnProgress; const cUrl, cFile: UTF8String);


implementation

uses
  fphttpclient;

procedure DonwloadFile(OnProgress: TOnProgress; const cUrl, cFile: UTF8String);
// cUrl  = 'http://www.imagemagick.org/download/binaries/ImageMagick-6.8.6-8-Q8-x86-static.exe';
// cFile = 'ImageMagick-6.8.6-8-Q8-x86-static.exe';
var
  vStream: TStreamAdapter;
  vHTTP: TFPHTTPClient;
  vSize: int64 = 0;
  i: integer;
  s: string;
begin
  try
    vHTTP := TFPHTTPClient.Create(nil);
    vHTTP.HTTPMethod('HEAD', cUrl, nil, [200]);

    for i := 0 to pred(vHTTP.ResponseHeaders.Count) do
    begin
      s := UpperCase(vHTTP.ResponseHeaders[I]);
      if Pos('CONTENT-LENGTH:', s) > 0 then
      begin
        vSize := StrToIntDef(Copy(s, Pos(':', s) + 1, Length(s)), 0);
        Break;
      end;
    end;

    vStream := TStreamAdapter.Create(TFileStream.Create(cFile, fmCreate), vSize);
    vStream.OnProgress := OnProgress;

    vHTTP.HTTPMethod('GET', cUrl, vStream, [200]);
  finally
    vHTTP.Free;
    vStream.Free;
  end;
end;

{ TStreamAdapter }

constructor TStreamAdapter.Create(AStream: TStream; ASize: int64);
begin
  inherited Create;
  FStream := AStream;
  fStream.Size := ASize;
  fStream.Position := 0;
end;

destructor TStreamAdapter.Destroy;
begin
  FStream.Free;
  inherited Destroy;
end;

function TStreamAdapter.Read(var Buffer; Count: longint): longint;
begin
  Result := FStream.Read(Buffer, Count);
  DoProgress;
end;

function TStreamAdapter.Write(const Buffer; Count: longint): longint;
begin
  Result := FStream.Write(Buffer, Count);
  DoProgress;
end;

function TStreamAdapter.Seek(Offset: longint; Origin: word): longint;
begin
  Result := FStream.Seek(Offset, Origin);
end;

procedure TStreamAdapter.DoProgress;
begin
  fPercent := Trunc((FStream.Position) / (FStream.Size) * 100);
  //WriteLn(FStream.Size);
  if Assigned(OnProgress) then
  begin
    OnProgress(self, FPercent);
  end;
end;

end.



