// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

unit mainform;

{$mode objfpc}{$H+}

interface

uses
  utils, downloader,

  Classes, SysUtils, DB, LazFileUtils, FileUtil, Forms, Controls,
  Graphics, Dialogs, StdCtrls, DBGrids, EditBtn,
  IniPropStorage, SdfData, ExtCtrls,

  // FPC trunk fileinfo reads exe resources as long as you register the appropriate units:
  fileinfo,
  {$IF defined(WINDOWS)}
  winpeimagereader {needed for reading EXE info}
  {$ELSEIF defined(DARWIN)}
  machoreader {reading MACH-O executables}
  {$ELSEIF defined(UNIX)}
  elfreader {for reading ELF executables}
  {$ENDIF};

type

  { TFormConverter }

  TFormConverter = class(TForm)
    ComponentsURL: TEdit;
    ct2lazButton: TButton;
    DownloadComponents: TCheckBox;
    DownloadComponentsLabel: TLabel;
    BottomPanel: TPanel;
    BottomRightPanel: TPanel;
    DownloadExamples: TCheckBox;
    DownloadExamplesLabel: TLabel;
    ExamplesURL: TEdit;
    ExtractComponents: TCheckBox;
    ExtractComponentsCmd: TEdit;
    ExtractComponentsLabel: TLabel;
    ExtractExamples: TCheckBox;
    ExtractExamplesCmd: TEdit;
    ExtractExamplesLabel: TLabel;
    IncludeFileMaskForNotQuotedStrings: TEdit;
    RenameFileMaskLazarus: TEdit;
    RenameFileMaskTyphon: TEdit;
    IncludeFileMaskForQuotedStrings: TEdit;
    InfoButton: TButton;
    laz2ctButton: TButton;
    LogMemo: TMemo;
    RenameFromToLabel: TLabel;
    ReplacementsDBGrid: TDBGrid;
    ReplaceNotQuotedStrings: TCheckBox;
    RenameFiles: TCheckBox;
    ReplaceNotQuotedStringsLabel: TLabel;
    RenameFilesLabel: TLabel;
    ReplaceQuotedStrings: TCheckBox;
    ReplaceQuotedStringsLabel: TLabel;
    SettingsGroupBox: TGroupBox;
    TopPanel: TPanel;
    SessionStorage: TIniPropStorage;
    Splitter1: TSplitter;
    DataSource: TDataSource;
    ReplacementsData: TSdfDataSet;
    ExtractDir: TDirectoryEdit;
    ExtractDirLabel: TLabel;
    procedure ct2lazButtonClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure InfoButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure StepCheckboxChangeState(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    LoadedFile: TStrings;
    QuoteReplacements: boolean;
    VersionStr: string;
    procedure ExecStepDownloadComponents;
    procedure ExecStepDownloadExamples;
    procedure ExecStepExtractComponents;
    procedure ExecStepExtractExamples;
    procedure ExecStepReplaceNotQuotedStrings;
    procedure ExecStepReplaceQuotedStrings;
    procedure ExecStepRenameFiles;
    procedure RecalcGridColumnWidhts;
  public
    function  ReplaceInFile(FullFileName: UTF8String): boolean;
    function  RenameSingleFile(FullFileName: UTF8String): boolean;
    procedure Progress(Sender: TObject; Percent: integer);
  end;

  TFastFileSearcher = class(TFileSearcher)
  private
    FileMaskList: TStringList;
    procedure CommaSepToSearchFileMask(CommaSeparatedStr: string);
  protected
    procedure DoFileFound; override;
  public
    FileCounter: integer;
    constructor Create;
    destructor  Destroy; override;
    procedure   FindAllFiles(SearchPath, CommaSepFileMask: String);
  end;

  TFileRenamer = class(TFastFileSearcher)
  private
    LazarusExtensionsList: TStringList;
    TyphonExtensionsList: TStringList;
    //function GetCommaSepCount(CommaSepWords: string): word;
    procedure CommaSepToStrList(CommaSeparatedStr: string; var StrList: TStringList);
  protected
    procedure DoFileFound; override;
  public
    constructor Create;
    destructor  Destroy; override;
  end;

type
  TTyphonDownloadType  = (ctdlComponents, ctdlExamples);
  TConversionDirection = (dirLazarusToTyphon, dirTyphonToLazarus);

var
  FormConverter:          TFormConverter;
  ConversionDirection:    TConversionDirection;
  Searcher:               TFastFileSearcher;
  Renamer:                TFileRenamer;
  FileDownloadPercentage: integer = -1;

implementation

{$R *.lfm}

{   TFormConverter }

procedure TFormConverter.FormCreate(Sender: TObject);
var
  FileVerInfo: TFileVersionInfo;
begin
  if not FileExists(SessionStorage.IniFileName) then
    Position := poScreenCenter;

  LoadedFile := TStringList.Create;
  Searcher   := TFastFileSearcher.Create;
  Renamer    := TFileRenamer.Create;
  Renamer.LazarusExtensionsList.CaseSensitive := False; // search for extensions must not be case sensitive
  Renamer.TyphonExtensionsList.CaseSensitive := False;

  with ReplacementsData do
  begin
    Active := False;
    //ClearFields;
    FileName := 'replacements.csv';
    FirstLineAsSchema := True;
    Active := True;
  end;
  LogMemo.DoubleBuffered := True;

  FileVerInfo := TFileVersionInfo.Create(nil);
  try // extract version info and put it into the caption
    FileVerInfo.FileName := Application.ExeName; // paramstr(0)
    FileVerInfo.ReadFileInfo;
    Caption := Format(Caption, [FileVerInfo.VersionStrings.Values['FileVersion']]);
    //LogMemo.Text := FileVerInfo.VersionStrings.Text;
    VersionStr := 'ct2laz v' + FileVerInfo.VersionStrings.Values['FileVersion'];
  finally
    FileVerInfo.Free;
  end;

  {$IFDEF UNIX}
  ExtractDir.Text           := GetUserDir + 'ct2laz';
  ExtractComponentsCmd.Text := '7z -y x %COMPONENTS typhon/components/pl_* -r -o%EXTRACTDIR -bb1';
  ExtractExamplesCmd.Text   := '7z -y x %EXAMPLES -r -o%EXTRACTDIR -bb1';
  {$ENDIF}
end;

procedure TFormConverter.FormDestroy(Sender: TObject);
begin
  Renamer.Free;
  Searcher.Free;
  LoadedFile.Free;
end;

procedure TFormConverter.FormShow(Sender: TObject);
begin
  RecalcGridColumnWidhts;
end;

procedure TFormConverter.RecalcGridColumnWidhts;
var
  i, w: integer;
begin
  try
    ReplacementsData.DisableControls;
    with ReplacementsDBGrid do
    begin // workaround to adjust column widths
      if (Columns.Count - 1) <> COL_DESCRIPTION then // little CSV file check
      begin
        if MessageDlg('CSV database file does not have correct number of columns!' + LineEnding + 'Download latest "replacements.csv" file!', mtError, [mbOK],0) = mrOk then
          Application.Terminate;
      end;
      AutoFillColumns := True;
      w := 0;
      for i := 0 to Columns.Count - 1 do
        w := w + Columns[i].Width;
      Caption := IntToStr(ReplacementsDBGrid.Width) + ', ' + IntToStr(w) + ', ' + IntToStr(i);
      AutoFillColumns := False;
      Columns[COL_REPLACE].Width     := Round(0.08 * w);
      Columns[COL_HANDLER].Width     := Round(0.08 * w);
      Columns[COL_LAZARUS].Width     := Round(0.15 * w);
      Columns[COL_TYPHON].Width      := Round(0.15 * w);
      Columns[COL_DESCRIPTION].Width := w - (Columns[COL_REPLACE].Width + Columns[COL_HANDLER].Width + Columns[COL_LAZARUS].Width + Columns[COL_TYPHON].Width);
    end;
  finally
    ReplacementsData.EnableControls;
  end;
end;

procedure TFormConverter.FormResize(Sender: TObject);
begin
  RecalcGridColumnWidhts;
end;

procedure TFormConverter.InfoButtonClick(Sender: TObject);
begin
  LogMemo.Clear;
  LogMemo.Append('ct2laz utility can convert CodeTyphon projects to Lazarus, and Lazarus projects to CodeTyphon.');
  LogMemo.Append('It can also download zillion component packages and project examples from PilotLogic CodeTyphon');
  LogMemo.Append('web site and execute needed source file transformations to make them compatible with Lazarus.');
  LogMemo.Append('You can also use CodeTyphon local files instead of downloaded ones if you prefer that way.');
  LogMemo.Append('Such transformed component packages can be installed into Lazarus, and most of the provided');
  LogMemo.Append('CodeOcean examples should work out of the box. One of the advantages of using CodeTyphon pl_*');
  LogMemo.Append('packages is that lots of them are trully cross platform and well tested on each supported platform.');
  LogMemo.Append('Package compatibility info for Win/Lin/BSD/Sol/Mac/GTK2/QT4/QT5/WinCE/x86/x86_64/Arm/Arm64');
  LogMemo.Append('supported platforms can be found here: http://www.pilotlogic.com/sitejoom/index.php/wiki?id=198');
  LogMemo.Append('Some CodeTyphon packages will only work with Windows, or only with trunk FPC and trunk Lazarus.');
  LogMemo.Append('');
  LogMemo.Append('Latest version with full sources can be downloaded from http://bitbucket.org/avra/ct2laz');
  {$IFDEF UNIX}
  LogMemo.Append('For archives extraction you will first need to install p7zip from your package manager.');
  {$ELSE}
  LogMemo.Append('For archives extraction you will need to download 7zip tool from http://www.7-zip.org');
  {$ENDIF}
  LogMemo.Append('ct2laz is released under Mozilla Public License 2.0 (MPL-2.0) https://www.mozilla.org/MPL/2.0');
  LogMemo.Append('License explained in plain English can be found at https://www.tldrlegal.com/l/mpl-2.0');
  LogMemo.Append('');
  LogMemo.Append('Made by Жељко Аврамовић (user Avra in Lazarus and CodeTyphon forums)');
end;

procedure TFormConverter.StepCheckboxChangeState(Sender: TObject);
begin // disable edit fields depending on checkbox selection
  if Sender = DownloadComponents then
    ComponentsURL.Enabled := DownloadComponents.Checked;

  if Sender = DownloadExamples then
    ExamplesURL.Enabled := DownloadExamples.Checked;

  if Sender = ExtractComponents then
    ExtractComponentsCmd.Enabled := ExtractComponents.Checked;

  if Sender = ExtractExamples then
    ExtractExamplesCmd.Enabled := ExtractExamples.Checked;

  if Sender = ReplaceQuotedStrings then
    IncludeFileMaskForQuotedStrings.Enabled := ReplaceQuotedStrings.Checked;

  if Sender = ReplaceNotQuotedStrings then
    IncludeFileMaskForNotQuotedStrings.Enabled := ReplaceNotQuotedStrings.Checked;

  if Sender = RenameFiles then
  begin
    RenameFileMaskLazarus.Enabled := RenameFiles.Checked;
    RenameFileMaskTyphon.Enabled  := RenameFiles.Checked;
  end;
end;

procedure TFormConverter.ExecStepDownloadComponents;
var
  Counter: word = 0;
  TryAnotherRound: boolean = true;
  FileURL: UTF8String;
  NumericExtension: UTF8String;
begin
  if DownloadComponents.Checked and (ConversionDirection = dirTyphonToLazarus) then
  begin // Step 1: Download components:
    LogStep(1, 'Downloading components started');
    LogTab('Settings: ' + ComponentsURL.Text);

    while TryAnotherRound do
    begin
      if Counter = 0 then
      begin
        NumericExtension := '';
        FileURL := Trim(ComponentsURL.Text);            // 1st download try is exact url without numeric extension
      end
      else
      begin
        NumericExtension := Format('.%.3d', [Counter]); // 2nd download try is exact url + '.001', 3rd is exact url + '.002'...
        FileURL := Trim(ComponentsURL.Text) + NumericExtension;
      end;
      LogTab('Downloading file ' + FileURL + ' to ' + GetComponentsFullFileName(NumericExtension));
      try
        LogTab('Download progress: '); // dummy line which will be overwritten with file download percentage
        DonwloadFile(@Progress, FileURL, GetComponentsFullFileName(NumericExtension));
      except
        on E: Exception do
        begin
          LogTabOverwrite('File ' + GetComponentsFullFileName(NumericExtension) + ' could not be downloaded or created. Error: ' + E.Message);
          if Counter <> 0 then
          begin
            TryAnotherRound := false;
            if Counter >= 2 then
              LogTab('All component archive files have been downloaded.');
          end;
        end;
      end;
      Inc(Counter);
    end;
    LogStep(1, 'Downloading components finished');
  end
  else
    LogStep(1, 'Downloading components skipped');
end;

procedure TFormConverter.ExecStepDownloadExamples;
var
  Counter: word = 0;
  TryAnotherRound: boolean = true;
  FileURL: UTF8String;
  NumericExtension: UTF8String;
begin
  if DownloadExamples.Checked and (ConversionDirection = dirTyphonToLazarus) then
  begin // Step 2: Download examples:
    LogStep(2, 'Downloading examples started');
    LogTab('Settings: ' + ExamplesURL.Text);

    while TryAnotherRound do
    begin
      if Counter = 0 then
      begin
        NumericExtension := '';
        FileURL := Trim(ExamplesURL.Text);              // 1st download try is exact url without numeric extension
      end
      else
      begin
        NumericExtension := Format('.%.3d', [Counter]); // 2nd download try is exact url + '.001', 3rd is exact url + '.002'...
        FileURL := Trim(ExamplesURL.Text) + NumericExtension;
      end;
      LogTab('Downloading file ' + FileURL + ' to ' + GetExamplesFullFileName(NumericExtension));
      try
        LogTab('Download progress: '); // dummy line which will be overwritten with file download percentage
        DonwloadFile(@Progress, FileURL, GetExamplesFullFileName(NumericExtension));
      except
        on E: Exception do
        begin
          LogTabOverwrite('File ' + GetExamplesFullFileName(NumericExtension) + ' could not be downloaded or created. Error: ' + E.Message);
          if Counter <> 0 then
          begin
            TryAnotherRound := false;
            if Counter >= 2 then
              LogTab('All example archive files have been downloaded.');
          end;
        end;
      end;
      Inc(Counter);
    end;
    LogStep(2, 'Downloading examples finished');
  end
  else
    LogStep(2, 'Downloading examples skipped');
end;

procedure TFormConverter.ExecStepExtractComponents;
var
  Cmd: UTF8String;
begin
  if ExtractComponents.Checked and (ConversionDirection = dirTyphonToLazarus) then
  begin // Step 3: Extract components:
    LogStep(3, 'Extracting components started');
    LogTab('Settings: ' + ExtractComponentsCmd.Text);
    Cmd := ReplaceMacros(ExtractComponentsCmd.Text);
    RunProcess(Cmd);
    LogStep(3, 'Extracting components finished');
  end
  else
    LogStep(3, 'Extracting components skipped');
end;

procedure TFormConverter.ExecStepExtractExamples;
var
  Cmd: UTF8String;
begin
  if ExtractExamples.Checked and (ConversionDirection = dirTyphonToLazarus) then
  begin // Step 4: Extract examples:
    LogStep(4, 'Extracting examples started');
    LogTab('Settings: ' + ExtractExamplesCmd.Text);
    Cmd := ReplaceMacros(ExtractExamplesCmd.Text);
    RunProcess(Cmd);
    LogStep(4, 'Extracting examples finished');
  end
  else
    LogStep(4, 'Extracting examples skipped');
end;

procedure TFormConverter.ExecStepReplaceQuotedStrings;
begin
  if ReplaceQuotedStrings.Checked then
  begin // Step 5: Replace quoted strings in files:
    LogStep(5, 'Replacing quoted strings in files started');
    LogTab('Settings: ' + IncludeFileMaskForQuotedStrings.Text);
    QuoteReplacements := True;
    Searcher.FindAllFiles(GetExtractDir, IncludeFileMaskForQuotedStrings.Text);
    LogStep(5, 'Replacing quoted strings in files finished');
  end
  else
    LogStep(5, 'Replacing quoted strings in files skipped');
end;

procedure TFormConverter.ExecStepReplaceNotQuotedStrings;
begin
  if ReplaceNotQuotedStrings.Checked then
  begin // Step 6: Replace non-quoted strings in files:
    LogStep(6, 'Replacing non-quoted strings in files started');
    LogTab('Settings: ' + IncludeFileMaskForNotQuotedStrings.Text);
    QuoteReplacements := False;
    Searcher.FileCounter := 0;
    Searcher.FindAllFiles(GetExtractDir, IncludeFileMaskForNotQuotedStrings.Text);
    LogStep(6, 'Replacing non-quoted strings in ' + IntToStr(Searcher.FileCounter) + ' files finished');
  end
  else
    LogStep(6, 'Replacing non-quoted strings in files skipped');
end;

procedure TFormConverter.ExecStepRenameFiles;
begin
  if RenameFiles.Checked then
  begin // Step 7: Rename files (introduced in CT 6.10+):
    LogStep(7, 'Renaming files started');
    LogTab('Settings: ' + RenameFileMaskLazarus.Text + ' <=> ' + RenameFileMaskTyphon.Text);

    Renamer.FileCounter := 0;
    Renamer.CommaSepToStrList(RenameFileMaskLazarus.Text, Renamer.LazarusExtensionsList); // fill StrList with file extensions
    Renamer.CommaSepToStrList(RenameFileMaskTyphon.Text,  Renamer.TyphonExtensionsList);  // fill StrList with file extensions
    if Renamer.LazarusExtensionsList.Count <> Renamer.TyphonExtensionsList.Count then
    begin
      LogTab('ERROR: Number of Lazarus and Typhon renaming file extensions do not match!');
      LogStep(7, 'Renaming files finished');
      Exit;
    end;

    if ConversionDirection = dirLazarusToTyphon then
      Renamer.FindAllFiles(GetExtractDir, RenameFileMaskLazarus.Text)
    else // ConversionDirection = dirTyphonToLazarus
      Renamer.FindAllFiles(GetExtractDir, RenameFileMaskTyphon.Text);
    LogStep(7, 'Renaming ' + IntToStr(Renamer.FileCounter) + ' files finished');
  end
  else
    LogStep(7, 'Renaming files skipped');
end;

procedure TFormConverter.ct2lazButtonClick(Sender: TObject);
begin // execute all checked conversion steps
  try
    Screen.Cursor := crHourGlass;
    ct2lazButton.Enabled := False;
    laz2ctButton.Enabled := False;
    InfoButton.Enabled   := False;
    LogMemo.Clear;
    LogDateTime(VersionStr);

    if (Sender as TButton).Name = 'laz2ctButton' then
    begin // 'laz2ctButton'
      ConversionDirection := dirLazarusToTyphon;
      LogDateTime('Lazarus to CodeTyphon conversion started (possible steps 5, 6 and 7)');
    end
    else
    begin // 'ct2lazButton'
      ConversionDirection := dirTyphonToLazarus;
      LogDateTime('CodeTyphon to Lazarus conversion started (possible steps 1, 2, 3, 4, 5, 6 and 7)');
    end;

    if not DirectoryExistsUTF8(GetExtractDir) then
      if ForceDirectoriesUTF8(GetExtractDir) then // create dir if it doesn't exist
        LogDateTime('Directory ' + GetExtractDir + ' created');

    LogTab('Working directory: ' + ExtractDir.Text);
    if IsBlankInPath(ExtractDir.Text) then
    begin
      LogTab('ERROR: Working directory must not contain spaces! Work aborted.');
      Exit;
    end;

    ExecStepDownloadComponents;      // Step 1: Download components
    ExecStepDownloadExamples;        // Step 2: Download examples
    ExecStepExtractComponents;       // Step 3: Extract components
    ExecStepExtractExamples;         // Step 4: Extract examples
    ExecStepReplaceQuotedStrings;    // Step 5: Replace quoted strings in files
    ExecStepReplaceNotQuotedStrings; // Step 6: Replace non-quoted strings in files
    ExecStepRenameFiles;             // Step 7: Rename files (introduced in CT 6.10+)
  finally
    ct2lazButton.Enabled := True;
    laz2ctButton.Enabled := True;
    InfoButton.Enabled   := True;
    LogDateTime('Conversion finished...');
    LogDateTime('Recommendation: You should search the net for original license info of each component you want to use!');
    LogMemo.Lines.SaveToFile('session.log');
    Screen.Cursor := crDefault;
  end;
end;

function TFormConverter.ReplaceInFile(FullFileName: UTF8String): boolean;
var
  Replaced, DoQuotedReplacements, DoWholeWordReplacementsOnly: boolean;
  TmpRecNo: integer;
  SearchStringColumn, ReplaceStringColumn: byte;
  HandlerFunc: integer;
begin
  LoadedFile.LoadFromFile(FullFileName);
  Replaced := False;

  if ConversionDirection = dirTyphonToLazarus then
  begin // convert typhon to lazarus
    SearchStringColumn  := COL_TYPHON;
    ReplaceStringColumn := COL_LAZARUS;
  end
  else
  begin // convert lazarus to typhon
    SearchStringColumn  := COL_LAZARUS;
    ReplaceStringColumn := COL_TYPHON;
  end;

  try
    TmpRecNo := ReplacementsData.RecNo;
    ReplacementsData.DisableControls;
    ReplacementsData.First;
    while not ReplacementsData.EOF do
    begin
      HandlerFunc := ReplacementsData.Fields[COL_HANDLER].AsInteger;
      DoWholeWordReplacementsOnly := not (HandlerFunc = HANDLER_FORCE_PARTIAL_STRING_REPLACE); // like replacing FTyphonCompilerOptions and TTyphonCompilerOptions to FLazCompilerOptions and TLazCompilerOptions when just TyphonCompilerOptions is mentioned for replacing with LazCompilerOptions
      DoQuotedReplacements := QuoteReplacements and (HandlerFunc <> HANDLER_FORCE_REPLACE_WITHOUT_QUOTES); // with this special handler we force replacements without quotes even in files that are normally intended only for quoted replacements (like 'adFCL"/>' to 'FCL"/>' in LPK files)
      if UpperCase(Trim(ReplacementsData.Fields[COL_REPLACE].AsString)) = 'Y' then // accept only letter 'Y' as TRUE value to process the record, otherwise skip record
        LoadedFile.Text := ReplaceStr(LoadedFile.Text,
                                      ReplacementsData.Fields[SearchStringColumn].AsString,
                                      ReplacementsData.Fields[ReplaceStringColumn].AsString,
                                      DoQuotedReplacements, DoWholeWordReplacementsOnly, Replaced);
      ReplacementsData.Next;
    end;
    if Replaced then
    begin
      LoadedFile.SaveToFile(FullFileName);
      Inc(Searcher.FileCounter);
    end;
  finally
    ReplacementsData.RecNo := TmpRecNo;
    ReplacementsData.EnableControls;
  end;

  Result := Replaced;
end;

function TFormConverter.RenameSingleFile(FullFileName: UTF8String): boolean;
var // rename file on file extension match
  Renamed: boolean;
  OldFileExtension, NewFileExtension, NewFullFileName: UTF8String;
  OldExtensionList, NewExtensionList: TStringList; // pointers to already existing StrLists, so no creation needed
  ExtIndex: integer;
begin
  Renamed := False;
  Result := False;

  if ConversionDirection = dirTyphonToLazarus then
  begin // rename typhon to lazarus file extension
    OldExtensionList := Renamer.TyphonExtensionsList;
    NewExtensionList := Renamer.LazarusExtensionsList;
  end
  else
  begin // rename lazarus to typhon file extension
    OldExtensionList := Renamer.LazarusExtensionsList;
    NewExtensionList := Renamer.TyphonExtensionsList;
  end;

  OldFileExtension := ExtractFileExt(FullFileName);
  ExtIndex := OldExtensionList.IndexOf(OldFileExtension);
  if ExtIndex < 0 then
  begin
    LogTab('ERROR: File extension not recognized for file ' + FullFileName);
    Exit;
  end;

  try
    NewFileExtension := NewExtensionList.Strings[ExtIndex];
    NewFullFileName  := ChangeFileExt(FullFileName, NewFileExtension);
    if FileExists(NewFullFileName) then
    begin
      LogTab('ERROR: File already exists so renaming is not possible to ' + NewFullFileName);
      Exit;
    end;
    Renamed := RenameFile(FullFileName, NewFullFileName);
    LogTab('New file extension ' + UpperCase(StringWithoutFirstChar(NewFileExtension)) + ' for ' + FullFileName);
    Inc(Renamer.FileCounter);
  except
    LogTab('ERROR: Problem renaming file ' + FullFileName);
  end;

  Result := Renamed;
end;

procedure TFormConverter.Progress(Sender: TObject; Percent: integer);
var
  i: integer;
  s: string;
begin // progress indicator for file download
  if FileDownloadPercentage <> Percent then
  begin
    FileDownloadPercentage := Percent;
    s := '';
    for i := 1 to (Percent div 10) do
      if i <> 10 then
        s := s + IntToStr(i * 10) + '...';
    LogTabOverwrite('Download progress: ' + s + IntToStr(Percent) + '%');
  end;
  Application.ProcessMessages;
end;


{ TFastFileSaercher }

constructor TFastFileSearcher.Create;
begin
  inherited Create;
  FileMaskList := TStringList.Create;
end;

destructor TFastFileSearcher.Destroy;
begin
  FileMaskList.Free;
  inherited Destroy;
end;

procedure TFastFileSearcher.DoFileFound;
begin // replace text inside of single matched file
  LogTab(FileName);
  FormConverter.ReplaceInFile(FileName);
  Application.ProcessMessages;
end;

procedure TFastFileSearcher.CommaSepToSearchFileMask(CommaSeparatedStr: string);
var // convert comma separated file masks into TStrings
  s: string;
begin
  FileMaskList.Clear;
  while Length(CommaSeparatedStr) > 0 do
  begin
    s := Copy2SymbDel(CommaSeparatedStr, ';');
    FileMaskList.Add(s);
  end;
end;

procedure TFastFileSearcher.FindAllFiles(SearchPath, CommaSepFileMask: string);
var
  i: integer;
begin
  CommaSepToSearchFileMask(CommaSepFileMask);
  for i := 0 to FileMaskList.Count - 1 do
    Search(SearchPath, FileMaskList[i]);
end;


{ TFileRenamer  }

constructor TFileRenamer.Create;
begin
  inherited Create;
  LazarusExtensionsList := TStringList.Create;
  TyphonExtensionsList  := TStringList.Create;
end;

destructor TFileRenamer.Destroy;
begin
  TyphonExtensionsList.Free;
  LazarusExtensionsList.Free;
  inherited Destroy;
end;

procedure TFileRenamer.DoFileFound;
begin // rename single matched file
  FormConverter.RenameSingleFile(FileName);
  Application.ProcessMessages;
end;

procedure TFileRenamer.CommaSepToStrList(CommaSeparatedStr: string; var StrList: TStringList);
begin // convert '*.ctpr; *.ppr; *.frm; *.ctpkg; *.pkgln' to StrList=('.ctpr','.ppr','.frm','.ctpkg','.pkgln')
  StrList.Clear;
  while Length(CommaSeparatedStr) > 0 do
    StrList.Add(ExtractFileExt(Copy2SymbDel(CommaSeparatedStr, ';')));
end;


end.
