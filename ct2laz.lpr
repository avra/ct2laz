// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

program ct2laz;

{$mode objfpc}{$H+}

{$ifdef Linux}
  {$ifdef FPC_CROSSCOMPILING}
    {$ifdef CPUARM}
      //if GUI on RPi, then uncomment
      //{$linklib GLESv2}
    {$endif}
    {$linklib libc_nonshared.a}
  {$endif}
{$endif}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Sysutils, Dialogs, Forms, mainform, sdflaz;

{$R *.res}

const
  REPLACEMENTS_FILENAME = 'replacements.csv'; // file with string replacements for CT2LAZ conversion

begin
  RequireDerivedFormResource := True;
  Application.Initialize;
  if not FileExists(ExtractFilePath(application.ExeName) + REPLACEMENTS_FILENAME) then // app path is needed here just in case executable is called from some other dir, so that we can always find CSV file
  begin
    MessageDlg('Fatal error', 'Application could not find file ''replacements.csv''.' + LineEnding + 'You need to download it from repository site!', mtError, [mbOK], 0);
    Exit;
  end;
  Application.CreateForm(TFormConverter, FormConverter);
  Application.Run;
end.

