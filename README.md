# **ct2laz** #

### Info ###

* ct2laz utility can convert [CodeTyphon](http://www.pilotlogic.com/sitejoom/) projects and component packages to [Lazarus](http://www.lazarus-ide.org/), and Lazarus projects and component packages to CodeTyphon.
* It can also download zillion component packages and project examples from CodeTyphon web site and execute needed source file transformations to make them compatible with Lazarus. You can use CodeTyphon local files instead of downloaded ones if you prefer that way. Such transformed component packages can be installed into Lazarus, and most of the provided CodeOcean examples should work out of the box. One of the advantages of using CodeTyphon pl_* packages is that many of them are trully cross platform and well tested on each supported platform. Package compatibility info for Win/Lin/BSD/Sol/Mac/GTK2/QT4/QT5/WinCE/x86/x86_64/Arm/Arm64 supported platforms can be found [here](http://www.pilotlogic.com/sitejoom/index.php/wiki?id=198). Some CodeTyphon packages will only work with trunk versions of FPC and Lazarus.
* For archives extraction you will need to download [7zip tool](http://www.7-zip.org) on Windows, or [p7zip package](http://p7zip.sourceforge.net) on Linux. 32-bit binary was tested on Windows XP SP3 32-bit, Windows 10 64-bit, Linux Debian 8 Cinammon 32 bit, Makulu 11 64 bit, Ubuntu Mate 15.10 32 bit, and Manjaro 16.06.1 32 bit.
* ct2laz Windows and Linux executables were compiled with Lazarus 1.8+ (only sdflaz package must be installed), but if you convert original Lazarus project with it's executable you will be able to compile with CodeTyphon 6.30+ too.
* If you press "CT => Laz" button without changing default settings, application will download latest CodeTyphon components and CodeOcean examples, and convert them for direct use in Lazarus. Table "replacements.csv" is the hearth of the conversion, as it holds all package name pairs that differ in Lazarus and CodeTyphon. If for any reason package names change or latest IDE versions bring some new packages, this is the place where you fix things. Then you can send me fixed CSV file, or let me know and I will fix changed/added package name pairs.
* By default, ct2laz will convert every known package name. But let's say that you want to use Typhoon's pl_indy instead of original indy Lazarus package, or pl_synapse instead of laz_synapse. That would mean that you would actually want to exclude those 2 packages from the conversion process. That is possible because all conversion pairs are stored in a CSV database table file. All you have to do is to locate their rows in CSV file and change T to F in first column of such rows in the table. That would tell ct2laz to skip such rows. You can do it from ct2laz (grid data is editable), or directly in some text editor.
* Since v6.10 CodeTyphon has changed default Lazarus file extensions, so ct2laz now also supports file renaming during conversion process. However because of breaking changes you must first delete old INI file and download latest CSV file before any conversion process.

### Screenshot ###
* ![ct2laz screenshot](https://bitbucket.org/avra/ct2laz/raw/master/screenshot.png)

### Download ###
* Linux and Windows binaries and CSV file can be found [here](https://bitbucket.org/avra/ct2laz/downloads)

### License ###
* ct2laz is released under [Mozilla Public License 2.0 (MPL-2.0)](https://www.mozilla.org/MPL/2.0)
* [License explained in plain English](https://www.tldrlegal.com/l/mpl-2.0)

### Author ###
* Made by Жељко Аврамовић (user Avra in Lazarus and CodeTyphon forums)

### Versions ###

* 1.4.0.230 Adapted download files naming to new CT 7.2+ 3 digit numeric increment file extension for components and examples archives, so download from Pilot Logic site works again. Fixed INI storage to also save rename files checkbox status between sessions.
* 1.2.3.224 Fixed session and multi project file handling (*.lps; *.ctps; *.lpg; and *.ctmpr were missing in form edit box). IT IS A MUST to delete your old INI file.
* 1.2.2.219 Fixed resource file handling (*.lrs and *.ctrs were missing in form edit box). Fixed output handling for new 7zip (added -bb1 switch which is not compatible with old 7zip v9.20 any more). Report error when old 7zip was found or when 7zip is not at expected location. Fixed disabling rename file edit boxes. Changed CSV boolean column values from 'T' and 'F' to more intuitive 'Y' and 'N'. These are breaking changes requiring you to delete old INI file and download new CSV file.
* 1.2.1.204 Fixed resource file renaming. Added more replacement pairs to CSV file.
* 1.2.0.201 Official 1.2 version. CT 6.30+ compatibility. Introduced function handlers and more replacements. Because of breaking changes IT IS A MUST to first delete your old INI file and download new REPLACEMENTS.CSV file before any conversion.
* 1.1.0.184 Added renaming files feature (CodeTyphon 6.10 and later changed several Lazarus file extensions). Because of breaking changes it is a must to first delete your old INI file before any conversion.
* 1.0.0.174 Updated broken download URL from CodeTyphon web site. Executable suffix now depends on architecture and target.
* 1.0.0.171 Official 1.0 version working on both Windows and Linux.
* 0.9.9.169 CSV file updated with package descriptions and latest packages. Code clean up and cosmetic changes.
* 0.9.8.164 First version fully working on Linux. Fixed Windows version typos after regression.
* 0.9.7.162 First Linux compilation with great help of JuhaManninen. GUI adapted to Linux. Grabbing Linux external process output.
* 0.9.5.154 Lazarus version is now default (if you want CT version then convert project with it's executable:).
* 0.9.5.151 Removed CodeTyphon specific components, show message on CSV file missing.
* 0.9.1.136 Initial public release.